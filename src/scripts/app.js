(() => {
'use strict';

const _body = document.querySelector('body');

// hide scroll
const hideScroll = () => {
	const _scrollWidth = document.documentElement.scrollWidth;
	const _screenWidth = screen.width;
	const _rightPadding = _screenWidth - _scrollWidth;
	_body.style.paddingRight = `${_rightPadding}` + 'px';
	document.documentElement.classList.add('no-scroll');
};

// show scroll in
const showScroll = () => {
	_body.removeAttribute('style');
	document.documentElement.classList.remove('no-scroll');
};

// carousel

	const _carousel = document.querySelector('.js-carousel');
	const _seats = document.querySelectorAll('.js-carouselItem');
	const _lastChild = _carousel.lastElementChild;
	_lastChild.classList.add('ref');

	// delegate events
	const delegate = (criteria, listener) => {
		return function(e) {
			const el = e.target;
			do {
				if (!criteria(el)) {
					continue;
				}
				e.delegateTarget = el;
				listener.call(this, e);
				return;
			} while ((el === el.parentNode));
		};
	};

	// event nav carousel
	const toggleFilter = elem => {
		return (elem instanceof HTMLElement) && elem.matches('.js-carouselNav');
	};

	// handler carousel
	const toggleHandler = ev => {
		ev.preventDefault();
		let _newSeat;
		const _el = document.querySelector('.ref');
		const _currSliderControl = ev.delegateTarget;

		const next = el => {
			if (el.nextElementSibling) {
				return el.nextElementSibling;
			} else {
				return _carousel.firstElementChild;
			}
		};

		const prev = el => {
			if (el.previousElementSibling) {
				return el.previousElementSibling;
			} else {
				return _carousel.lastElementChild;
			}
		};

		_el.classList.remove('ref');
		if (_currSliderControl.getAttribute('data-nav') === 'next') {
			_newSeat = next(_el);
			_carousel.classList.remove('revers');
		} else {
			_newSeat = prev(_el);
			_carousel.classList.add('revers');
		}

		_newSeat.classList.add('ref');
		_newSeat.style.order = 1;
		for (let i = 2; i <= _seats.length; i++) {
			_newSeat = next(_newSeat);
			_newSeat.style.order = i;
		}

		_carousel.classList.remove('set');
		return setTimeout(() => {
			return _carousel.classList.add('set');
		}, 50);
	};

	// event view carousel
	const toggleNav = elem => {
		return (elem instanceof HTMLElement) && elem.matches('.js-viewImg');
	};

	// open view modal
	const _modal = document.querySelector('.js-carouselView');

	const toggleViewModal = ev => {
		ev.preventDefault();
		const _target = ev.target || ev.currentTarget;
		const _parent = _target.parentNode.parentNode;
		const _img = _parent.querySelector('img');
		const _src = _img.getAttribute('src');
		const _modalImg = _modal.querySelector('img');
		_modalImg.setAttribute('src', _src);
		_modal.classList.add('active');
		hideScroll();
	};

	// close view modal
	const closeViewModalBtn = ev => {
		ev.preventDefault();
		_modal.classList.remove('active');
		showScroll();
	};

	const toggleModal = elem => {
		return (elem instanceof HTMLElement) && elem.matches('.js-carouselViewClose')
			|| (elem instanceof HTMLElement) && elem.matches('.carouselView--wrap');
	};

	document.addEventListener("click", delegate(toggleFilter, toggleHandler));
	document.addEventListener("click", delegate(toggleNav, toggleViewModal));
	document.addEventListener("click", delegate(toggleModal, closeViewModalBtn));

})();